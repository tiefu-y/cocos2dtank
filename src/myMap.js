/**
 * Created with JetBrains WebStorm.
 * User: fancylou
 * Date: 13-6-14
 * Time: 下午8:47
 * Description: 测试使用tmx地图
 */

var g_GameZOrder = {bg:0, ui:1 , front:100};


var myLayer = cc.Layer.extend({

    watermelon:null,//西瓜
    wall:null,//墙
    map_0:null,

    ctor:function(){

        var texture =cc.TextureCache.getInstance().addImage(s_bullet_bomb);//首先将图片放入缓存
        var texture_tank_bomb =cc.TextureCache.getInstance().addImage(s_tank_bomb);//首先将图片放入缓存
        var texture_tank_before=cc.TextureCache.getInstance().addImage(s_tank_before);//首先将图片放入缓存

        this._super();

        this.map_0 =  cc.TMXTiledMap.create(s_wall01_tmx);
        this.addChild(this.map_0, g_GameZOrder.bg);
//        this.map_0.setAnchorPoint(cc.p(0,0));
//        this.map_0.setPosition(cc.p(0,0));

        this.wall =this.map_0.getLayer("wall");


        this.watermelon = new watermelonSprite();
        this.addChild(this.watermelon, g_GameZOrder.ui);

        this.watermelon.beginRotate();




        ///bullet_bomb动画测试
        var frame1 = cc.SpriteFrame.createWithTexture(texture, cc.RectMake(32 * 1, 32 * 0, 32, 32));
        var frame2 = cc.SpriteFrame.createWithTexture(texture, cc.RectMake(32 * 2, 32 * 0, 32, 32));
        var frame3 = cc.SpriteFrame.createWithTexture(texture, cc.RectMake(32 * 3, 32 * 0, 32, 32));

        var y=cc.RANDOM_0_1() * 132;
        var sprite = cc.Sprite.createWithSpriteFrame(frame1);
        this.addChild(sprite, g_GameZOrder.ui);
        sprite.setPosition(cc.p(55, 100));

        var animFrames = [];//将所有帧存入一个数组
        animFrames.push(frame1);
        animFrames.push(frame2);
        animFrames.push(frame3);
        var animation = cc.Animation.create(animFrames, 0.3);//将所有的动画帧，以间隔0.3秒速度播放
        var animate = cc.Animate.create(animation);
        var rep = cc.RepeatForever.create(animate);
        sprite.runAction(rep);

        //tank_bomb 测试
        var frame_tank_bomb1 = cc.SpriteFrame.createWithTexture(texture_tank_bomb, cc.RectMake(66 * 0, 66 * 0, 66, 66));
        var frame_tank_bomb2 = cc.SpriteFrame.createWithTexture(texture_tank_bomb, cc.RectMake(66 * 1, 66 * 0, 66, 66));
        var frame_tank_bomb3 = cc.SpriteFrame.createWithTexture(texture_tank_bomb, cc.RectMake(66 * 2, 66 * 0, 66, 66));
        var frame_tank_bomb4 = cc.SpriteFrame.createWithTexture(texture_tank_bomb, cc.RectMake(66 * 3, 66 * 0, 66, 66));

        //var y=cc.RANDOM_0_1() * 132;
        var sprite_tank_bomb = cc.Sprite.createWithSpriteFrame(frame_tank_bomb1);
        this.addChild(sprite_tank_bomb, g_GameZOrder.ui);
        sprite_tank_bomb.setPosition(cc.p(55, 182));

        var animFrames_tank_bomb = [];//将所有帧存入一个数组
        animFrames_tank_bomb.push(frame_tank_bomb1);
        animFrames_tank_bomb.push(frame_tank_bomb2);
        animFrames_tank_bomb.push(frame_tank_bomb3);
        animFrames_tank_bomb.push(frame_tank_bomb4);
        var animation_tank_bomb = cc.Animation.create(animFrames_tank_bomb, 0.3);//将所有的动画帧，以间隔0.3秒速度播放
        var animate_tank_bomb = cc.Animate.create(animation_tank_bomb);
        var rep_tank_bomb = cc.RepeatForever.create(animate_tank_bomb);
        sprite_tank_bomb.runAction(rep_tank_bomb);


        //texture_tank_before 测试
        var frame_tank_before1 = cc.SpriteFrame.createWithTexture(texture_tank_before, cc.RectMake(32 * 1, 32 * 0, 32, 32));
        var frame_tank_before2 = cc.SpriteFrame.createWithTexture(texture_tank_before, cc.RectMake(32 * 2, 32 * 0, 32, 32));
        var frame_tank_before3 = cc.SpriteFrame.createWithTexture(texture_tank_before, cc.RectMake(32 * 3, 32 * 0, 32, 32));
        var frame_tank_before4 = cc.SpriteFrame.createWithTexture(texture_tank_before, cc.RectMake(32 * 4, 32 * 0, 32, 32));
        var frame_tank_before5 = cc.SpriteFrame.createWithTexture(texture_tank_before, cc.RectMake(32 * 5, 32 * 0, 32, 32));
        var frame_tank_before6 = cc.SpriteFrame.createWithTexture(texture_tank_before, cc.RectMake(32 * 6, 32 * 0, 32, 32));
        var frame_tank_before7 = cc.SpriteFrame.createWithTexture(texture_tank_before, cc.RectMake(32 * 7, 32 * 0, 32, 32));

        //var y=cc.RANDOM_0_1() * 132;
        var sprite_tank_before = cc.Sprite.createWithSpriteFrame(frame_tank_before1);
        this.addChild(sprite_tank_before, g_GameZOrder.ui);
        sprite_tank_before.setPosition(cc.p(55, 252));

        var animFrames_tank_before = [];//将所有帧存入一个数组
        animFrames_tank_before.push(frame_tank_before1);
        animFrames_tank_before.push(frame_tank_before2);
        animFrames_tank_before.push(frame_tank_before3);
        animFrames_tank_before.push(frame_tank_before4);
        animFrames_tank_before.push(frame_tank_before5);
        animFrames_tank_before.push(frame_tank_before6);
        animFrames_tank_before.push(frame_tank_before7);
        var animation_tank_before = cc.Animation.create(animFrames_tank_before, 0.3);//将所有的动画帧，以间隔0.3秒速度播放
        var animate_tank_before = cc.Animate.create(animation_tank_before);
        var rep_tank_before = cc.RepeatForever.create(animate_tank_before);
        sprite_tank_before.runAction(rep_tank_before);




        this.schedule(this.update, 0);//参数1：执行函数，参数2：调用间隔时间，0为每帧都调用

    },
    update:function(dt){
        this.watermelon.update(dt);

        var pos = this.watermelon.getPosition();
        // cc.log("x:"+pos.x+"  , y:"+pos.y);
        var x = parseInt(pos.x/16);
        var y = parseInt((16*26-pos.y)/16);
        var p = new cc.Point(x,y);
        var gid = this.wall.getTileGIDAt(p);
        //cc.log("gid:"+gid);
        if(gid>0){
            var properites = this.map_0.propertiesForGID(gid);
            if(properites){
                var value = properites['isBlock'];
                //cc.log("pro:"+value);
            }else{
                cc.log("properties is empty");
            }
        }

    }
});


var myMapeScene =  cc.Scene.extend({


    onEnter:function(){


        this._super();

        this.gameLayer = new myLayer();
        this.addChild(this.gameLayer);


    }

});