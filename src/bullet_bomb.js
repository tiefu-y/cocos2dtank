/**
 * Created with JetBrains WebStorm.
 * User: fancylou
 * Date: 13-6-26
 * Time: 下午7:51
 * Description: 生成坦克前的动画
 */


var bulletBomb = cc.Sprite.extend({

    active:true,//状态

    ctor:function(){
        var texture =cc.TextureCache.getInstance().addImage(s_bullet_bomb);//首先将图片放入缓存
        this._super();
        ///bullet_bomb动画测试
        var frame1 = cc.SpriteFrame.createWithTexture(texture, cc.RectMake(32 * 1, 32 * 0, 32, 32));
        var frame2 = cc.SpriteFrame.createWithTexture(texture, cc.RectMake(32 * 2, 32 * 0, 32, 32));
        var frame3 = cc.SpriteFrame.createWithTexture(texture, cc.RectMake(32 * 3, 32 * 0, 32, 32));
        var animFrames = [];//将所有帧存入一个数组
        animFrames.push(frame1);
        animFrames.push(frame2);
        animFrames.push(frame3);
        var animation = cc.Animation.create(animFrames, 0.1);//将所有的动画帧，以间隔0.2秒速度播放
        cc.AnimationCache.getInstance().addAnimation(animation,"animation_bullet_bomb");



       var animation01 = cc.AnimationCache.getInstance().getAnimation("animation_bullet_bomb");
        var animate = cc.Animate.create(animation01);
        this.runAction(cc.Sequence.create(
            animate,
            cc.CallFunc.create(this.destroy, this)
        ));
    },
    destroy:function(){
        this.active = false;
        var pos = this.getPosition();
        this.setPosition(g_hideSpritePos);
    }
});


bulletBomb.getOrCreateBulletBomb = function(){
    for(var i=0 ; i < bullet_bomb_animation_array.length ; i++){
        var selfChild = bullet_bomb_animation_array[i];
        if(selfChild.active==false){
            selfChild.active = true;
            var animation01 = cc.AnimationCache.getInstance().getAnimation("animation_bullet_bomb");
            var animate = cc.Animate.create(animation01);
            selfChild.runAction(cc.Sequence.create(
                animate,
                cc.CallFunc.create(selfChild.destroy, selfChild)
            ));

            return selfChild;
        }
    }

    var bomb = new bulletBomb();
    bullet_bomb_animation_array.push(bomb);
    g_wallLayer_obj.addBulletBomb(bomb);
    return bomb;
}