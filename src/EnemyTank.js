/**
 * Created with JetBrains WebStorm.
 * User: fancylou
 * Date: 13-6-20
 * Time: 下午4:58
 * Description: 敌军坦克 所有的 都在这里
 */



/**
 * 敌军坦克基础类，所有敌军坦克都基础这个类
 * @type {Function}
 */
var baseEnemyTank = cc.Sprite.extend({

    radius:16,//碰撞半径
    velocity:cc.p(0,0),//初始速度
    direction:t_direction.south,//初始方向
    HP:1,
    type:0,
    active:true,
    speed:tank_speed_default,
    turnInteval:0,

    ctor:function(arg){
        this._super();
        this.initWithFile(arg.file);
        this.HP = arg.HP;
        this.type = arg.type;
        this.speed = arg.speed;
        this.velocity = cc.p(0,(-1 * this.speed));
    },
    quadrangle:function(){     //计算子弹的4个角
        var pos = this.getPosition();
        var qua = [];
        var q1 =cc.p((pos.x-16),(pos.y+16));
        var q2 = cc.p((pos.x-16),(pos.y-16));
        var q3 = cc.p((pos.x+16),(pos.y+16));
        var q4 = cc.p((pos.x+16),(pos.y-16));
        qua.push(q1);
        qua.push(q2);
        qua.push(q3);
        qua.push(q4);
        return qua;
    },
    update:function(dt){
        if(this.turnInteval>3){
            this.randomDirection();
        }
        var isHit = this.checkHitEdge();
        if(!isHit){
            this.randomDirection();
        }else{
            if(this.collideWall()){
                this.randomDirection();
            }else{
                this.setPosition(cc.pAdd(this.getPosition(), cc.pMult(this.velocity, dt)));
            }
        }

        if(this.HP<=0){
            this.destroy();
        }
    },
    destroy:function(){
        this.active = false;
        this.setPosition(g_hideSpritePos);
        this.stopAllActions();
    },
    shoot:function(){
        var arg = {type:bullet_type.enemy,direction:this.direction};
        var b = bullet.getOrCreateBullet(arg);
        b.setPosition(this.getPosition());

        this.turnInteval ++;//每秒加一
    },
    //边界碰撞
    checkHitEdge:function(){
        var pos = this.getPosition();
        var contentSize = this.getContentSize();
        var winSize = cc.Director.getInstance().getWinSize();
        if (this.direction==t_direction.east && pos.x >= winSize.width - this.radius) {//右边界
            return false;
        }
        if (this.direction==t_direction.west &&pos.x <= this.radius) {//左边界
            return false;
        }
        if (this.direction==t_direction.south && pos.y <= this.radius) {//下边界
            return false;
        }
        if (this.direction==t_direction.north && pos.y >= winSize.height - this.radius) {//上边界
            return false;
        }

        return true;
    },
    calculateDistance:function(qua1, qua2){//循环比较2个对象的4个角距离是否
        for(var i = 0; i < qua1.length ; i++){
            var p1 = qua1[i];
            var p21 = qua2[0];
            var p22 = qua2[1];
            var p23 = qua2[2];
            var p24 = qua2[3];
            if((p21.x<=p1.x)&&(p1.x <= p23.x)){
                if((p21.y>=p1.y)&&(p22.y<=p1.y)){
                    return true;
                }
            }

        }
        return false;
    },
    //碰撞检测
    collideWall: function() {
        var qua= this.quadrangle();
        var redWall =  g_wallLayer_obj.tiledRedWallArray;
        var kingkong = g_wallLayer_obj.tiledKingKongWallArray;

        for(var j =0 ; j<redWall.length ; j++){
            var w = redWall[j];
            var wSprite = w.sp;
            var wPos = wSprite.getPosition();
            var wQua = [];
            var q1 =cc.p((wPos.x-8),(wPos.y+8));
            var q2 = cc.p((wPos.x-8),(wPos.y-8));
            var q3 = cc.p((wPos.x+8),(wPos.y+8));
            var q4 = cc.p((wPos.x+8),(wPos.y-8));
            wQua.push(q1);
            wQua.push(q2);
            wQua.push(q3);
            wQua.push(q4);
            var ishit = this.calculateDistance(qua, wQua);
            if(ishit){
                return true;
            }
        }
        for(var j =0 ; j<kingkong.length ; j++){
            var w = kingkong[j];
            var wSprite = w.sp;
            var wPos = wSprite.getPosition();
            var wQua = [];
            var q1 =cc.p((wPos.x-8),(wPos.y+8));
            var q2 = cc.p((wPos.x-8),(wPos.y-8));
            var q3 = cc.p((wPos.x+8),(wPos.y+8));
            var q4 = cc.p((wPos.x+8),(wPos.y-8));
            wQua.push(q1);
            wQua.push(q2);
            wQua.push(q3);
            wQua.push(q4);
            var ishit = this.calculateDistance(qua, wQua);
            if(ishit){
                return true;
            }
        }

        return false;
    },
    /**
     * 随机改变方向，排除当前方向
     * 
     * @param currentDirection 当前方向
     */
    randomDirection:function(){
        this.turnInteval = 0;
        var id = parseInt(Math.random()*4);
        var flag = true;
        while(flag){
            if(id!=this.direction){
                flag = false;
            }else{
                id = parseInt(Math.random()*4);
            }
        }
        if(id == t_direction.south){
            this.velocity = cc.p(0,(-1*this.speed));
            this.countAngle(t_direction.south);
            this.direction = t_direction.south;
        }
        if(id == t_direction.north){
            this.velocity = cc.p(0,this.speed);
            this.countAngle(t_direction.north);
            this.direction = t_direction.north;
        }
        if(id == t_direction.east){
            this.velocity = cc.p(this.speed,0);
            this.countAngle(t_direction.east);
            this.direction = t_direction.east;
        }
        if(id == t_direction.west){
            this.velocity = cc.p((-1*this.speed),0);
            this.countAngle(t_direction.west);
            this.direction = t_direction.west;
        }
    },

    /**
     * 坦克旋转
     * @param newDirection
     */
    countAngle:function(newDirection){
        var normalAngle = 90;
        switch (newDirection){
            case t_direction.south:
                normalAngle = 0;
                break;
            case t_direction.west:
                normalAngle = 90;
                break;
            case t_direction.north:
                normalAngle = 180;
                break;
            case t_direction.east:
                normalAngle = 270;
                break

        }

        var rotate = cc.RotateTo.create(0.1, normalAngle);
        this.runAction(rotate);
    }

});


baseEnemyTank.getOrCreateEnemyTank = function(arg) {
    for (var j = 0; j < enemy_tank_array.length; j++) {
        selChild = enemy_tank_array[j];
        if (selChild.active == false && selChild.type == arg.type)
        {
            selChild.HP = arg.HP;
            selChild.speed = arg.speed;
            selChild.active = true;
            return selChild;
        }
    }

    var addEnemy = new baseEnemyTank(arg);
    g_wallLayer_obj.addTank(addEnemy);
    enemy_tank_array.push(addEnemy);
    return addEnemy;
};

