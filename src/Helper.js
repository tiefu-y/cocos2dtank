/**
 * Created with JetBrains WebStorm.
 * User: fancylou
 * Date: 13-7-25
 * Time: 下午10:07
 * To change this template use File | Settings | File Templates.
 */

Array.prototype.remove = function(arg){
    var i=0,n=0;
    var arrSize = this.length;
    for(i=0;i<arrSize;i++){
        if(this[i] != arg){
            this[n++]=this[i];
        }
    }
    if(n<i){
        this.length = n;
    }
};
/**
 *     根据下标删除数组元素
 *     @param index
 */
Array.prototype.removeByIndex = function(index){
    var i=0,n=0;
    var arrSize = this.length;
    for(i=0;i<arrSize;i++){
        if(this[i] != this[index]){
            this[n++]=this[i];
        }
    }
    if(n<i){
        this.length = n;
    }
};


Array.prototype.contain = function(arg){
    var i=0;
    var arrSize = this.length;
    for(i=0;i<arrSize;i++){
        if(this[i] == arg){
            return true;
        }
    }
    return false;
};