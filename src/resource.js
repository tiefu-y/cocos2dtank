var s_HelloWorld = "res/HelloWorld.png";
var s_CloseNormal = "res/CloseNormal.png";
var s_CloseSelected = "res/CloseSelected.png";
var s_watermelon = "res/watermelon.png";


var s_tank1 = "res/tank-1.png";
var s_tank2 = "res/tank-2.png";
var s_tank3 = "res/tank-3.png";
var s_tank4 = "res/tank-4.png";
var s_tank5 = "res/tank-5.png";
var s_tank6 = "res/tank-6.png";
var s_tank7 = "res/tank-7.png";
var s_tank_protect = "res/tank_protect.png";
var s_bullet = "res/bullet.png";
var s_bullet_bomb = "res/bullet_bomb.png";
var s_tank_bomb = "res/tank_bomb.png";
var s_tank_before = "res/tank_before.png";
var s_home = "res/home.png";
var s_home_destroy = "res/home_destroy.png";



var s_Konqa32FNT = "res/konqa32.fnt";
var s_Konqa32PNG = "res/konqa32.PNG";
var s_Konqa32HDFNT = "res/konqa32-hd.fnt";
var s_Konqa32HDPNG = "res/konqa32-hd.PNG";

var s_wall01_tmx = "res/wall01.tmx";

var g_ressources = [
    //image
    {type:"image", src:s_HelloWorld},
    {type:"image", src:s_CloseNormal},
    {type:"image", src:s_CloseSelected},
    {type:"image", src:s_watermelon},

    {type:"image", src:s_tank1},
    {type:"image", src:s_tank2},
    {type:"image", src:s_tank3},
    {type:"image", src:s_tank4},
    {type:"image", src:s_tank5},
    {type:"image", src:s_tank6},
    {type:"image", src:s_tank7},
    {type:"image", src:s_tank_protect},
    {type:"image", src:s_bullet},
    {type:"image", src:s_bullet_bomb},
    {type:"image", src:s_tank_bomb},
    {type:"image", src:s_tank_before},
    {type:"image", src:s_home},
    {type:"image", src:s_home_destroy},

    {type:'image', src:s_Konqa32PNG},
    {type:'image', src:s_Konqa32HDPNG},


    //plist

    //fnt
    {type:'fnt', src:s_Konqa32FNT},
    {type:'fnt', src:s_Konqa32HDFNT},
    //tmx
    {type:"tmx", src:s_wall01_tmx}
    //bgm

    //effect
];