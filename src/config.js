/**
 * Created with JetBrains WebStorm.
 * User: fancylou
 * Date: 13-6-26
 * Time: 下午7:59
 * Description: 配置参数
 */


var g_hideSpritePos = cc.p(-10,-10);

var t_direction = {south:0,west:1,north:2,east:3};//方向

var tank_speed_default = 64;

var g_enemyTank_born = [
    cc.p(16,400),
    cc.p(208,400),
    cc.p(400,400)
];//敌军坦克出生地点

var tank_before_array = [] ; //坦克生成前动画组


var enemy_tank_type = [
    {type:0,HP:1,file:s_tank3,speed:(tank_speed_default*1.5)},
    {type:1,HP:2,file:s_tank4,speed:(tank_speed_default*1)},
    {type:2,HP:3,file:s_tank5,speed:(tank_speed_default*1)},
    {type:3,HP:4,file:s_tank6,speed:(tank_speed_default*0.5)},
    {type:4,HP:5,file:s_tank7,speed:(tank_speed_default*0.5)}
];//敌军坦克类型

var enemy_tank_array = [];//敌军坦克
var bullet_array = [];//子弹
var bullet_bomb_animation_array = [];//子弹爆炸动画