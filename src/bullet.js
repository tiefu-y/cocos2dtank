/**
 * Created with JetBrains WebStorm.
 * User: fancylou
 * Date: 13-6-20
 * Time: 下午8:20
 * Description: 子弹对象
 */

var bullet_type = {player:1,enemy:2};
var bullet = cc.Sprite.extend({
    radius:3,//碰撞半径
    velocity:cc.p(0,200),//初始速度
    active:true,
    type:bullet_type.player,//子弹属于谁
    direction:t_direction.north,//子弹方向


    ctor:function(type, direction){
        this._super();
        this.initWithFile(s_bullet);
        this.type = type;
        this.rotateInit(direction);
    },
    quadrangle:function(){     //计算子弹的4个角
        var pos = this.getPosition();
        var qua = [];
        var q1 =cc.p((pos.x-3),(pos.y+3));
        var q2 = cc.p((pos.x-3),(pos.y-3));
        var q3 = cc.p((pos.x+3),(pos.y+3));
        var q4 = cc.p((pos.x+3),(pos.y-3));
        qua.push(q1);
        qua.push(q2);
        qua.push(q3);
        qua.push(q4);
        return qua;
    },
    update:function(dt){
        var isHit = this.checkHitEdge();
        if(!isHit){
            this.destroy();
        }
        //TODO
        this.collide();

        if(this.active==true){
            this.setPosition(cc.pAdd(this.getPosition(), cc.pMult(this.velocity, dt)));
        }
    },

    destroy:function(){
        this.active = false;
        var pos = this.getPosition();
        this.setPosition(g_hideSpritePos);//移除子弹
        //播放爆炸动画
        var bomb = bulletBomb.getOrCreateBulletBomb();
        bomb.setAnchorPoint(cc.p(0.5,0.5));
        bomb.setPosition(pos);
    },
    calculateDistance:function(qua1, qua2){//循环比较2个对象的4个角距离是否
           for(var i = 0; i < qua1.length ; i++){
               var p1 = qua1[i];
               var p21 = qua2[0];
               var p22 = qua2[1];
               var p23 = qua2[2];
               var p24 = qua2[3];
               if((p21.x<=p1.x)&&(p1.x <= p23.x)){
                   if((p21.y>=p1.y)&&(p22.y<=p1.y)){
                        return true;
                   }
               }

           }
        return false;
    },
    //碰撞检测 墙面和坦克
    collide:function(){
        var hit = false;
        //地图碰撞 碰到墙面爆炸
        var qua= this.quadrangle();
        var redWall =  g_wallLayer_obj.tiledRedWallArray;
        for(var j =0 ; j<redWall.length ; j++){
            var w = redWall[j];
            var wSprite = w.sp;
            var wPos = wSprite.getPosition();
            var wQua = [];
            var q1 =cc.p((wPos.x-8),(wPos.y+8));
            var q2 = cc.p((wPos.x-8),(wPos.y-8));
            var q3 = cc.p((wPos.x+8),(wPos.y+8));
            var q4 = cc.p((wPos.x+8),(wPos.y-8));
            wQua.push(q1);
            wQua.push(q2);
            wQua.push(q3);
            wQua.push(q4);
            var ishit = this.calculateDistance(qua, wQua);
            if(ishit){
                hit = true;
                g_wallLayer_obj.tmxLayer.removeTileAt(w.pos);
                redWall.removeByIndex(j);
                j--;
            }
        }
        g_wallLayer_obj.tiledRedWallArray = redWall;
        if(hit){
            this.destroy();
        }

        //TODO 坦克碰撞

    },

    //边界碰撞
    checkHitEdge:function(){
        var pos = this.getPosition();
        var contentSize = this.getContentSize();
        var winSize = cc.Director.getInstance().getWinSize();
        if (this.direction==t_direction.east && pos.x >= winSize.width - this.radius - 3) {//右边界
            return false;
        }
        if (this.direction==t_direction.west &&pos.x <= this.radius + 3) {//左边界
            return false;
        }
        if (this.direction==t_direction.south && pos.y <= this.radius + 3) {//下边界
            return false;
        }
        if (this.direction==t_direction.north && pos.y >= winSize.height - this.radius -3) {//上边界
            return false;
        }

        return true;
    },

    rotateInit:function(di){
        var normalAngle = 90;
        switch (di){
            case t_direction.south:
                this.direction = t_direction.south;
                normalAngle = 180;
                this.velocity = cc.p(0,-200);
                break;
            case t_direction.west:
                this.direction = t_direction.west;
                normalAngle = 270;
                this.velocity = cc.p(-200,0);
                break;
            case t_direction.north:
                this.direction = t_direction.north;
                normalAngle = 360;
                this.velocity = cc.p(0,200);
                break;
            case t_direction.east:
                this.direction = t_direction.east;
                normalAngle = 90;
                this.velocity = cc.p(200,0);
                break

        }
        var rotate = cc.RotateTo.create(0.1, normalAngle);
        this.runAction(rotate);
    }

});


bullet.getOrCreateBullet = function(arg) {
    for(var i = 0; i < bullet_array.length ; i++){
        selChild = bullet_array[i];
        if (selChild.active == false && selChild.type == arg.type)
        {
            selChild.active = true;
            selChild.rotateInit(arg.direction);
            return selChild;
        }
    }

    var bt = new bullet(arg.type,arg.direction);
    g_wallLayer_obj.addBullet(bt);
    bullet_array.push(bt);
    return bt;
}