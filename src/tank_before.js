/**
 * Created with JetBrains WebStorm.
 * User: fancylou
 * Date: 13-6-26
 * Time: 下午7:51
 * Description: 生成坦克前的动画
 */


var EnemyTankBefore = cc.Sprite.extend({

    active:true,//状态

    ctor:function(){
//        cc.associateWithNative( this, cc.Sprite );
        var texture_tank_before = cc.TextureCache.getInstance().addImage(s_tank_before);//首先将图片放入缓存

        this._super();
        //texture_tank_before 动画
        var frame_tank_before1 = cc.SpriteFrame.createWithTexture(texture_tank_before, cc.RectMake(32 * 1, 32 * 0, 32, 32));
        var frame_tank_before2 = cc.SpriteFrame.createWithTexture(texture_tank_before, cc.RectMake(32 * 2, 32 * 0, 32, 32));
        var frame_tank_before3 = cc.SpriteFrame.createWithTexture(texture_tank_before, cc.RectMake(32 * 3, 32 * 0, 32, 32));
        var frame_tank_before4 = cc.SpriteFrame.createWithTexture(texture_tank_before, cc.RectMake(32 * 4, 32 * 0, 32, 32));
        var frame_tank_before5 = cc.SpriteFrame.createWithTexture(texture_tank_before, cc.RectMake(32 * 5, 32 * 0, 32, 32));
        var frame_tank_before6 = cc.SpriteFrame.createWithTexture(texture_tank_before, cc.RectMake(32 * 6, 32 * 0, 32, 32));
        var frame_tank_before7 = cc.SpriteFrame.createWithTexture(texture_tank_before, cc.RectMake(32 * 7, 32 * 0, 32, 32));
        var animFrames_tank_before = [];//将所有帧存入一个数组
        animFrames_tank_before.push(frame_tank_before1);
        animFrames_tank_before.push(frame_tank_before2);
        animFrames_tank_before.push(frame_tank_before3);
        animFrames_tank_before.push(frame_tank_before4);
        animFrames_tank_before.push(frame_tank_before5);
        animFrames_tank_before.push(frame_tank_before6);
        animFrames_tank_before.push(frame_tank_before7);
        var animation_tank_before = cc.Animation.create(animFrames_tank_before, 0.1);//将所有的动画帧，以间隔0.3秒速度播放
        cc.AnimationCache.getInstance().addAnimation(animation_tank_before,"animation_tank_before");



       var animation01 = cc.AnimationCache.getInstance().getAnimation("animation_tank_before");
        var animate_tank_before = cc.Animate.create(animation01);
        this.runAction(cc.Sequence.create(
            animate_tank_before,
            cc.CallFunc.create(this.destroy, this)
        ));
    },
    destroy:function(){
        this.active = false;
        var pos = this.getPosition();
        this.setPosition(g_hideSpritePos);

        var type = parseInt(cc.RANDOM_0_1() * 5);
        var enemy = baseEnemyTank.getOrCreateEnemyTank(enemy_tank_type[type]);
        enemy.setPosition(pos);
    }
});


EnemyTankBefore.getOrCreateEnemyTankBefore = function(){
    for(var i=0 ; i < tank_before_array.length ; i++){
        var selfChild = tank_before_array[i];
        if(selfChild.active==false){
            selfChild.active = true;
            var animation01 = cc.AnimationCache.getInstance().getAnimation("animation_tank_before");
            var animate_tank_before = cc.Animate.create(animation01);
            selfChild.runAction(cc.Sequence.create(
                animate_tank_before,
                cc.CallFunc.create(selfChild.destroy, selfChild)
            ));

            return selfChild;
        }
    }

    var enTbefore = new EnemyTankBefore();
    tank_before_array.push(enTbefore);
    g_wallLayer_obj.addTankBefore(enTbefore);
    return enTbefore;
}