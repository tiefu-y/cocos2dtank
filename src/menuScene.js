/**
 * Created with JetBrains WebStorm.
 * User: fancylou
 * Date: 13-6-18
 * Time: 下午8:23
 * Description: 开始菜单页面
 */

var g_MenuZOrder = {bg:0, ui:1 , front:100};


var menuLayer = cc.Layer.extend({

    background:null,
    director:null,
    winSize:null,
    centerPos:null,
    Z_LABEL :30,

    ctor:function(){
        this._super();
//        this.background = cc.TMXTiledMap.create(s_menu_tmx);
//        this.addChild(this.background, g_MenuZOrder.bg);
        //        this.background.setAnchorPoint(cc.p(0,0));
        //        this.background.setPosition(cc.p(0,0));

        this.director = cc.Director.getInstance();
        this.winSize = this.director.getWinSize();
        this.centerPos = cc.p(this.winSize.width / 2, 2 * this.winSize.height / 3);

        //battle city  title
        var label = cc.LabelBMFont.create("BATTLE CITY", s_Konqa32HDFNT);
        label.setPosition(this.centerPos);
        this.addChild(label, g_MenuZOrder.ui);
        label.setScale(0.2);
        var sa = cc.ScaleTo.create(0.5, 1.05);
        var sb1 = cc.ScaleTo.create(0.5, 1);
        var sb2 = cc.ScaleTo.create(0.5, 1.05);
        var seq = cc.Sequence.create(sb1, sb2);
        var rep = cc.Repeat.create(seq, 1000);
        var all = cc.Sequence.create(sa, rep);
        label.runAction(all);


        //menu
        cc.MenuItemFont.setFontSize(16 * 2);
        var item1 = cc.MenuItemFont.create("1 Player", this.onRestart, this);
        var item2 = cc.MenuItemFont.create("2 Players", this.onMainMenu, this);
        var menu = cc.Menu.create(item1, item2);
        menu.alignItemsVertically();
        this.addChild(menu, g_MenuZOrder.ui);
        menu.setPosition(this.winSize.width / 2, this.winSize.height / 3);
    },

    onRestart:function(sender){

        var scene = cc.Scene.create();
        scene.addChild(wallLayer.create(),1);
        cc.Director.getInstance().replaceScene(cc.TransitionFade.create(1.2, scene));

//        var scene = new battleScene();
//        this.director.replaceScene(cc.TransitionSlideInT.create(0.5, scene));
    },

    onMainMenu:function(sender){

    }
});

var MenuScene = cc.Scene.extend({

    gameLayer:null,

    onEnter:function(){
        this._super();
        this.gameLayer = new menuLayer();
        this.addChild(this.gameLayer);

    }


});
