/**
 * Created with JetBrains WebStorm.
 * User: fancylou
 * Date: 13-6-23
 * Time: 下午8:23
 * Description: 战斗场景
 */

var g_BattleSceneOrder = {bg:-9, ui:1 , front:100};
var g_wallLayer_obj ;

var wallLayer = cc.Layer.extend({
    wall:null,//墙--地图
    tmxLayer:null,
    home_base:null,
    _time:0,
    showTank:false,
    tiledRedWallArray:[],//红墙
    tiledKingKongWallArray:[],//金刚墙


    ctor:function(){
//        this._super();

    },
    init:function(){
        var bRet = false;
        if(this._super()){
            this.wall =  cc.TMXTiledMap.create(s_wall01_tmx);
            this.addChild(this.wall, -9);
            this.tmxLayer = this.wall.getLayer("wall");
            //获取所有的红墙和金钢墙
            for(var i=0;i<26;i++){
                for(var j=0;j<26;j++){
                    var p = new cc.Point(i,j);
                    var gid = this.tmxLayer.getTileGIDAt(p);
                    if(gid>0){
                        var properites = this.wall.propertiesForGID(gid);
                        if(properites){
                            var block = properites['isBlock'];
                            var hit = properites['isHit'];
                            if(block=="true" && hit =="true"){
                                var redObj = new Object();
                                redObj.sp = this.tmxLayer.getTileAt(p);
                                redObj.pos = p;
                                this.tiledRedWallArray.push(redObj);
                            }else if(block=="false" && hit =="true"){
                                var kingkong = new Object();
                                kingkong.sp = this.tmxLayer.getTileAt(p);
                                kingkong.pos = p;
                                this.tiledKingKongWallArray.push(kingkong);
                            }
                        }
                    }

                }
            }
            var objects = this.wall.getObjectGroup("base");
            var gledeBase = objects.objectNamed('gledeBase');
            var x = gledeBase.x;
            var y = gledeBase.y;
            x = x + 16;
            this.home_base = cc.Sprite.create(s_home);
            this.home_base.setPosition(cc.p(x,y));
            this.addChild(this.home_base);
            g_wallLayer_obj = this;

            this.schedule(this.update, 0);//参数1：执行函数，参数2：调用间隔时间，0为每帧都调用
            this.schedule(this.timeCount, 1);

            bRet = true;
        }


        return bRet;
    },
    update:function(dt){

        //敌军坦克
        for(var i = 0; i < enemy_tank_array.length ; i++){
            var etank = enemy_tank_array[i];
            if(etank.active==true){
                etank.update(dt);
            }
        }

        //子弹
        for(var b=0; b < bullet_array.length; b++){
            var bu = bullet_array[b];
            if(bu.active==true){
                bu.update(dt);
            }
        }


    },
    //每秒检查坦克
    timeCount:function(){
        var enemy_num = 0;
        this._time++;
        var minute = 0 | (this._time / 60);
        var second = this._time % 60;
        var showTankTime = this._time % 2;
        if(showTankTime == 0){
            this.showTank = true;
        }else{
            this.showTank = false;
        }
        //敌军坦克
        for(var i = 0; i < enemy_tank_array.length ; i++){
            var etank = enemy_tank_array[i];
            if(etank.active==true){
                etank.shoot();//发射子弹
                enemy_num ++;
            }
        }
        if(enemy_num < 6 && this.showTank == true){
            var pos= parseInt(cc.RANDOM_0_1() * 3);
            var enemy_before = EnemyTankBefore.getOrCreateEnemyTankBefore();
            enemy_before.setAnchorPoint(cc.p(0.5,0.5));
            enemy_before.setPosition(g_enemyTank_born[pos]);
        }



    },

    addTankBefore:function(b){
        this.addChild(b,g_BattleSceneOrder.ui);
    },
    addTank:function(t){
        this.addChild(t,g_BattleSceneOrder.ui);
    },
    addBullet:function(bt){
        this.addChild(bt,g_BattleSceneOrder.ui);
    },
    addBulletBomb:function(bomb){
        this.addChild(bomb,g_BattleSceneOrder.ui);
    }





});



wallLayer.create = function () {
    var sg = new wallLayer();
    if (sg && sg.init()) {
        return sg;
    }
    return null;
};

wallLayer.scene = function () {
    var scene = cc.Scene.create();
    var layer = wallLayer.create();
    scene.addChild(layer, 1);
    return scene;
};

//var battleScene = cc.Scene.extend({
//
//    onEnter:function(){
//        this._super();
//        var gamelay = new wallLayer();
//        gamelay.init();
//        this.addChild(gamelay,1);
//    }
//});
